<?php

namespace App\Http\Controllers;

use App\Correction;
use App\Http\Controllers\Main;
use DB;
use Request;

class CorrectionsController extends Main {

    /**
     * index
     * Una corrección es el motivo de una cancelación.
     * Devuelve todas las correciones después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * 
     * @return response NULL|Internal Server Error(500)
     */
    public function index() {
        
        try {

            $corrections = DB::table('corrections');

            foreach(Request::query() as $name => $value) {
                
                $corrections = $corrections->where($name, $value);

            }

            return Main::response(true, null, $corrections->get());

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', null, 500);

        }
   
    }

    /**
     * show
     * Muestra una correccion (\App\Cancellation) por medio del ID de cancelacion.
     *
     * @\App\Cancellation
     * 
     * @param  int      $id ID de la cancelacion
     * @return response     NULL|Not Found(404)
     */
    public function show($id) {

        if($correction = Cancellation::find($id)) {

            return Main::response(true, 'OK', $correction);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

}