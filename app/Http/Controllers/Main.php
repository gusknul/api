<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Client;
use DB;
use Request;
use PDO;
use DateTime;
use DateInterval;
use LaravelAnalytics;

class Main extends Controller {

	public function __construct(array $except = array())
	{

		$this->middleware(
			'auth',
			[
				'except' => array_merge($except, ['getToken', 'postLogin', 'postRegister', 'postEmail', 'postReset', 'on', 'unregistered', 'sendPassword', 'job', 'registerCommissions', 'canceledSubscriptions', 'loginFacebook', 'registerFacebook', 'getRegisterFacebook', 'template', 'invoice'])
			]
		);

		$this->middleware(
				'csrf',
				[
						'except' => ['on', 'sendPassword', 'job', 'invoice', 'registerCommissions', 'canceledSubscriptions', 'registerFacebook', 'getRegisterFacebook', 'loginFacebook', 'template','generate']
				]
		);

		}

		public function response($success, $message, $object, $status = 200, $headers = array())
		{
			return response()->json([
						'success' => $success,
						'message' => $message,
						'response' => $object
					], $status, $headers);

		}

	public function dashboard()
	{
		$params = Request::query();
		$from = isset($params['from']) ? $params['from'] : date('Y-01-01');
		$to   = isset($params['to'])   ? $params['to']   : date('Y-12-31');

		$social = "SELECT DATE_FORMAT(charges.paid_at, '%Y-%m') AS date,
						ROUND(SUM(charges.amount) / 100, 2) AS amount,
						SUM(CASE
										WHEN charges.description = 'Pago tardío' THEN clients.social_responsability * ROUND((debts.amount - ROUND(debts.amount / (1 + (1 / 1.16 * .007)), 0)) * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)) / 100, 2)
										WHEN charges.description = 'Extras' THEN clients.social_responsability * ROUND(((charges.amount - clients.deposit) - ROUND((charges.amount - clients.deposit) / (1 + (1 / 1.16 * .007)), 0)) * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)) / 100, 2)
										WHEN charges.description = 'Cargos adicionales Aguagente' THEN 0
										WHEN charges.description = 'Depósito' THEN 0
										ELSE clients.social_responsability * ROUND((charges.amount - ROUND(charges.amount / (1 + (1 / 1.16 * .007)), 0)) * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)) / 100, 2)
								END) AS client_social,
						SUM(CASE
										WHEN charges.description = 'Pago tardío' THEN ROUND(IF( clients.social_responsability, (charges.amount - (debts.amount - ROUND(debts.amount / (1 + (1 / 1.16 * .007)), 0))) / 1.16 * .007 * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)), charges.amount / 1.16 * .007 * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)) ) / 100, 2 )
										WHEN charges.description = 'Extras' THEN ROUND( IF( clients.social_responsability, ((charges.amount - clients.deposit) - ROUND((charges.amount - clients.deposit) / (1 + (1 / 1.16 * .007)), 0)) * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)), (charges.amount - clients.deposit) / 1.16 * .007 * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)) ) / 100, 2 )
										WHEN charges.description = 'Cargos adicionales Aguagente' THEN ROUND(charges.amount / 1.16 * .007 * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)) / 100, 2)
										WHEN charges.description = 'Depósito' THEN 0
										ELSE ROUND( IF( clients.social_responsability, (charges.amount - ROUND(charges.amount / (1 + (1 / 1.16 * .007)), 0)) * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)), charges.amount / 1.16 * .007 * (1 - ROUND(IFNULL(refunds.refunded_amount, 0) / charges.amount, 2)) ) / 100, 2 )
								END) AS aguagente_social
				FROM charges
				INNER JOIN clients ON clients.id_clients = charges.id_clients
				LEFT JOIN debts ON debts.id_charges = charges.id_charges
				LEFT JOIN
					( SELECT id_charges,
									 SUM(amount) AS refunded_amount
					 FROM refunds
					 GROUP BY id_charges ) AS refunds ON charges.id_charges = refunds.id_charges
				WHERE charges.paid_at IS NOT NULL &&
					charges.paid_at <= ? &&
				charges.paid_at >= ?
				GROUP BY date";

		$connection = new PDO('mysql:host='.env('DB_HOST').';dbname='.env('DB_DATABASE'), env('DB_USERNAME'), env('DB_PASSWORD'));
		$statement = $connection->prepare($social);
		$statement->bindValue(1, $to);
		$statement->bindValue(2, $from);
		$statement->execute();
		$selectedIncomes = $statement->fetchAll(PDO::FETCH_OBJ);

		$statement = $connection->prepare($social);
		$statement->bindValue(1, '3000');
		$statement->bindValue(2, '2000');
		$statement->execute();

		$totals = array_reduce(
			$statement->fetchAll(PDO::FETCH_OBJ),
			function($totals, $income) {
				$totals['income'] += $income->amount;
				$totals['client_social'] += $income->client_social;
				$totals['aguagente_social'] += $income->aguagente_social;
				return $totals;
			},
			['income' => 0, 'client_social' => 0, 'aguagente_social' => 0]
		);

		$statement = $connection->prepare("SELECT count(*) as quantity, status FROM tickets GROUP BY status");
		$statement->execute();
		$tickets = $statement->fetchAll(PDO::FETCH_OBJ);

		$statement = $connection->prepare("SELECT count(*) as quantity, status, SUM(IF(serial_number IS NULL, 0, 1)) AS installed, SUM(IF(subscription_status IS NULL, 0, 1)) AS charged FROM clients GROUP BY status");
		$statement->execute();
		$clients = $statement->fetchAll(PDO::FETCH_OBJ);

		$statement = $connection->prepare("SELECT count(*) as quantity, date_format(created_at, '%Y-%m') as month, CONCAT('subscription.canceled') as type FROM `cancellations` WHERE created_at <= ? && created_at >= ? group by month");
		$statement->bindValue(1, $to);
		$statement->bindValue(2, $from);
		$statement->execute();
		$events = $statement->fetchAll(PDO::FETCH_OBJ);

		$statement = $connection->prepare("SELECT * FROM withdrawals");
		$statement->execute();
		$withdrawals = $statement->fetchAll(PDO::FETCH_OBJ);

		$statement = $connection->prepare("SELECT ROUND(SUM(amount + collection_fees + moratory_fees) / 100, 2) as amount FROM debts WHERE id_charges IS NULL");
		$statement->execute();
		$totals['debts'] = (float)$statement->fetchColumn();

		$newSuscriptions = Client::select(DB::raw('COUNT(*) AS quantity, date_format(collected_at, "%Y-%m") as month'))
									->whereBetween('collected_at', [$from, $to])
									->where('status', '=', 'accepted')
                                    ->whereNotNull('pay_day')
									->orderBy('month')
									->groupBy(DB::raw('MONTH(collected_at)'))
									->get()
									->toArray();

		return [
			'totals'              => $totals,
			'incomes'             => $selectedIncomes,
			'clients'             => $clients,
			'subscription_events' => $events,
			'newSuscriptions'     => $newSuscriptions,
			'tickets'             => [
				'opened' => array_reduce($tickets, function($q, $ticket) { return $ticket->status == 'opened' ? ($q + $ticket->quantity) : $q; }, 0),
				'closed' => array_reduce($tickets, function($q, $ticket) { return $ticket->status == 'closed' ? ($q + $ticket->quantity) : $q; }, 0),
				'assigned' => array_reduce($tickets, function($q, $ticket) { return $ticket->status == 'assigned' ? ($q + $ticket->quantity) : $q; }, 0),
				'confirmed' => array_reduce($tickets, function($q, $ticket) { return $ticket->status == 'confirmed' ? ($q + $ticket->quantity) : $q; }, 0),
				'completed' => array_reduce($tickets, function($q, $ticket) { return $ticket->status == 'completed' ? ($q + $ticket->quantity) : $q; }, 0)
			],
			'withdrawals' => $withdrawals,
			'general' => LaravelAnalytics::performQuery(new DateTime($from), new DateTime($to), 'ga:users,ga:sessions,ga:screenviews,ga:screenviewsPerSession,ga:avgSessionDuration,ga:percentNewSessions')->rows,
			'views' => LaravelAnalytics::performQuery(new DateTime($from), new DateTime($to), 'ga:screenviews,ga:uniqueScreenviews,ga:avgScreenviewDuration,ga:exitRate', $others = array('dimensions'=>'ga:screenName','sort'=>'-ga:screenviews', 'filters'=>'ga:screenName!~www.*;ga:screenName!~panel.*;ga:ScreenName!~localhost'))->rows
		];

	}

}
