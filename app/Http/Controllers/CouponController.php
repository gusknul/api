<?php

namespace App\Http\Controllers;

use App\Coupon;
use Request;
use Validator;
use DB;

use App\User;
use App\Role;
use App\Client;


class CouponController extends Main {

    /**
     * __construct
     * Se le indica que la funcion "generate" no debe usar AUTH, por medio del controlador MAIN
     */
	public function __construct() {

        parent::__construct(['generate']);

    }

    /**
     * index
     * Devuelve todas los cupones después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * 
     * @return response OK
     */
    public function index() {

    	$cupones = DB::table('coupon');

		foreach(Request::query() as $name => $value) {

            $cupones = $cupones->where($name, $value);

        }

        $cupones = $cupones->get();

		return Main::response(true, 'OK', $cupones, 200);
    }

    /**
     * generate
     * Genera un cupon de recompenza por una cantidad monetaria, dependiendo de el nivel completado.
     * EL CUPON SOLO LO PUEDE GENERAR DESDE LA IP 138.197.207.96 (web del juego "Aguar llega a la meta")
     * 
     * @return response NULL|Bad request|Credencial incorrecta|Error(400)
     */
    public function generate(){

    	try {

            $input = Request::all();

            $validator = Validator::make(
                $input,
                [
                    'email'  => 'required|email',
                    'level' => 'required|integer'
                ]
            );

            if($validator->fails()) {

                return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

            }

            if(Request::ip() != '138.197.207.96'){
                return Main::response(false, 'Credencial incorrecta', null, 401);
            }

            $rewards = array(
            	2 => '$100',
                3 => '$200',
                4 => '$499',
                5 => '$799',
                6 => '$999',
                7 => '$999',
                8 => '$999',
                9 => '$999',
                10 => '$999',
                11 => '$999',
                12 => '$999',
                13 => '$999',
                14 => '$999',
                15 => '$999',
                16 => '$999',
                17 => '$999',
                18 => '$999',
                19 => '$999',
                20 => '$999'
            );

            $cupon = new Coupon;
    		$cupon->email = $input['email'];
    		$cupon->token = uniqid();
    		$cupon->level =	$input['level'];
    		$cupon->reward = $rewards[(int)$input['level']];
    		$cupon->user = 0;
            $cupon->save();

            return Main::response(true, null, $cupon, 201);

        } catch(\Exception $e) {

            return Main::response(false, $e->getMessage(), null, 400);

        }

    }

    /**
     * manuallyStatusClient
     * Busca un cliente (\App\Client) por medio de su ID($id).
     * Si el nivel es "Viajero", se desactiva como agente de ventas (sales_agent) y se le quita el rol.
     * Si no, y el cliente no está como agente de ventas, se le asigna el rol.
     * 
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Client
     * @App\User
     * 
     * @param  int      $id ID de cliente
     * @return response     OK|Bad request|Not Found(404)
     */
    public function manuallyStatusClient($id){

        if( $client = Client::find($id) ) {

            try{

                $input = Request::all();

                $validator = Validator::make(
                    $input,
                    [
                        'level'  => 'required'
                    ]
                );

                if( $validator->fails() ) {

                    return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

                }

                $user = User::find($client->id_users);

                if( $input['level'] == 'VIAJERO' ){

                    DB::table('role_user')->where('user_id', $client->id_users)
                                          ->where('role_id', 5)
                                          ->delete();

                    $client->level = 'VIAJERO';
                    $client->sales_agent = 0;
                    $client->save();

                    return Main::response(true, 'OK', $client, 200);

                }

                if(!$user->hasRole('Sales Agent'))
                    $user->roles()->attach(5);

                $client->level = $input['level'];
                $client->sales_agent = 1;
                $client->save();

                return Main::response(true, 'OK', $client, 200);

            }
            catch(\Exception $e){

                return Main::response(false, $e->getMessage(), null, 400);

            }

        }
        else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

}
