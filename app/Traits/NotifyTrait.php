<?php
namespace App\Traits;

use App\Debt;
use App\Contract;
use App\Client;
use App\Charge;
use Request;
use Mail;

trait NotifyTrait
{
    /**
     * $staff
     *
     * @var array
     */
    protected $staff = [
        'servicio@guagente.com',
        'raiders11@gmail.com'
    ];

    /**
     * notifyUserOffline
     * Envía un email con la referencia generada para su pago OFFLINE.
     * 
     * @param string $type  Tipo de notificación(OXXO/SPEI)
     * @param object $order Objeto de CONEKTA con la respuesta del cargo.
     * @param Client $client
     * @return void
     */
    public function notifyUserOffline($type, $order, $client)
    {
        $view = 'email.oxxo';
        $data = [
            'reference' => $order->charges[0]->payment_method->reference,
            'total'     => $order->amount/100
        ];

        if ($type == 'SPEI') {
            $view              = 'email.spei';
            $data['reference'] = $order->charges[0]->payment_method->clabe;
            $data['bank']      = $order->charges[0]->payment_method->bank;
        }

        $to = $this->staff;

        Mail::send($view, $data, function ($message) use ($client, $to){
            $message->subject('Referencia de pago');
            //$message->to($client->email);
            $message->to($to);
        });
    }

    /**
     * notifyStaffOffilinePayment
     * Se le envía un email al staff cuando un pago offline se realice exitosamente. 
     * 
     * @param array $params Array de parametros
     * @return void
     */
    public function notifyStaffOffilinePayment($params)
    {
        $to = $this->staff;

        Mail::send('email.offline_payment', $params, function ($message) use ($to) {
            $message->subject('Pago offline exitoso');
            $message->to($to);
        });
    }
}