<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Correction extends Model {

	protected $table = 'corrections';
	protected $primaryKey = 'id_corrections';
	
}