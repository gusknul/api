<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Element extends Model {

	protected $table = 'categories_elements';
	protected $primaryKey = 'id_categories_elements';
	
}

