<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rejection extends Model {

	protected $table = 'rejections';
	protected $primaryKey = 'id_rejections';
	
}