<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\DailyCharge::class,
        \App\Console\Commands\CalculateCommissions::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('inspire')->everyMinute()->appendOutputTo(storage_path().'/logs/cron.log');

        $schedule->command('charges:run')->dailyAt('00:05')->appendOutputTo(storage_path().'/logs/cron.log');
        $schedule->command('commission:calculate')
                 ->dailyAt('23:50')
                 ->when(function () {
                      return \Carbon\Carbon::now()->endOfMonth()->isToday();
                 })
                 ->appendOutputTo(storage_path().'/logs/commission.log');
    }
}
