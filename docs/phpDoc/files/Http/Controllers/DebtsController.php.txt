<?php

namespace App\Http\Controllers;

use App\Debt;
use App\Contract;
use App\Client;
use App\MySQLClientRepository;
use App\LevelService;
use Request;
use Conekta;
use Conekta_Charge;
use Conekta_Customer;
use Conekta_Plan;
use Conekta_ProcessingError;
use Conekta_ParameterValidationError;

class DebtsController extends Main {

    /**
     * index
     * Devuelve todas las deudas (\App\Debt) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Debt
     * 
     * @return response OK|Internal Server Error
     */
    public function index() {

        try {

            $debts = Debt::query();

            foreach(Request::query() as $name => $value) {

                switch($name) {
                    case 'status':
                        if($value == 'unpaid')
                            $debts = $debts->whereNull('id_charges');
                        else
                            $debts = $debts->whereNotNull('id_charges');
                    break;
                    default:
                        $debts = $debts->where($name, $value);
                    break;
                }

            }

            return Main::response(true, 'OK', $debts->get(), 200);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }

    }

    /**
     * show
     * Muestra una deuda (\App\Debt) por medio del ID
     *
     * @\App\Debt
     * 
     * @param  int      $id ID de la deuda
     * @return response     OK|Not Found(404)
     */
    public function show($id) {

        if($debt = Debt::find($id)) {

            return Main::response(true, 'Ok', $debt, 200);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

    /**
     * charge
     * Intenta realizar un cobro via CONEKTA a un cliente(\App\Client).
     * El cliente se busca mediante el id_clients de la deuda.
     * Si se logra realizar el cobro, se le reduce el monto cobrado a la deuda y se le asigna a la deuda el 
     *   ID de transaccion(CONEKTA) del cobro.
     *
     * @Conekta
     * @Conekta_Customer
     * @Conekta_Charge
     * @\App\Client
     * @\App\Debt
     * 
     * @param  int      $id ID de la deuda
     * @return response     OK|Payment Required|Not Found|Internal Server Error
     */
    public function charge($id) {

        try {

            if($debt = Debt::find($id)) {

                if($debt->id_charges) {

                    return Main::response(false, 'Forbidden', 'Debt has been paid', 403);

                }

                $client = Client::find($debt->id_clients);

                Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
                Conekta::setLocale('es');

                $customer = Conekta_Customer::find($client->conekta_token);

                $cards = json_decode($customer->cards->__toJSON());

                foreach($cards as $i => $card)
                    if($card->id == $customer->default_card_id) {
                        unset($cards[$i]);
                        array_unshift($cards, $card);
                        break;
                    }

                foreach($cards as $card) {

                    try {

                        $charge = Conekta_Charge::create(
                            array(
                                'description' => 'Pago tardío',
                                'amount' => $debt->amount + $debt->collection_fees + $debt->moratory_fees,
                                'currency' => 'MXN',
                                'card' => $card->id,
                                'details' => array(
                                    'name' => $client->name,
                                    'phone' => $client->phone,
                                    'email' => $client->email,
                                    'line_items' => array(
                                        array(
                                            'name' => 'Suscripción',
                                            'description' => 'Suscripción',
                                            'unit_price' => $debt->amount,
                                            'quantity' => 1,
                                            'type' => 'digital'
                                        ),
                                        array(
                                            'name' => 'Gastos de cobranza',
                                            'description' => 'Gastos de cobranza',
                                            'unit_price' => $debt->collection_fees,
                                            'quantity' => 1,
                                            'type' => 'digital'
                                        ),
                                        array(
                                            'name' => 'Interés moratorio',
                                            'description' => 'Interés moratorio',
                                            'unit_price' => $debt->moratory_fees,
                                            'quantity' => 1,
                                            'type' => 'digital'
                                        )
                                    ),
                                    'shipment' => array(
                                        'carrier' => 'aguagente',
                                        'service' => 'next_day',
                                        'price' => 0,
                                        'address' => array(
                                            'street1' => $client->address,
                                            'street2' => $client->address,
                                            'street3' => $client->address,
                                            'city' => $client->county,
                                            'state' => $client->state,
                                            'zip' => $client->postal_code,
                                            'country' => 'Mexico'
                                        )
                                    )
                                )
                            )
                        );

                        $debt->id_charges = $charge->id;
                        $debt->save();

                        $client->debt -= $charge->amount;
                        $client->save();

                        $levelService = new LevelService(new MySQLClientRepository());
                        $levelService->setLevels($client);

                        return Main::response(true, 'Ok', $debt, 200);

                    } catch(\Exception $e) {

                        // do nothing

                    }

                }

                return Main::response(false, 'Payment Required',  ['errors' => ['charge' => ['No fue posible realizar el cargo']]], 402);

            } else {

                return Main::response(false, 'Not Found', null, 404);

            }

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }

    }

}

