<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Refund extends Model {

	protected $table = 'refunds';
	protected $primaryKey = 'id_refunds';

}

