<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model {

	protected $table = 'devices';
	protected $primaryKey = 'id_devices';

}

